﻿using LaunchPads;
using Testing;

class Program {
  
    // Main Method
    static public void Main(String[] args)
    {
        // Create launchpad builder
        ILaunchPadBuilder lp = new LaunchPadBuilder(LaunchPadType.BASIC);
        
        // Set launchpad attributes
        lp.EnableHorizontalButtons(5);
        lp.EnableVerticalButtons(5);
        lp.EnableUSBTypeCPort();

        // Build launchpad
        LaunchPad basic = lp.BuildLaunchPad();
        lp.Reset(LaunchPadType.BASIC);

        // Clone launchpad
        LaunchPad basic2 = basic.clone();
        LaunchPad basic3 = basic.clone();

        TestingFacade tf = new TestingFacade(basic, basic2, basic3);
    }
}