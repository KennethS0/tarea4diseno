using LaunchPads;

namespace Testing{

///This is the tester class, which the user can use to verify the functionality of the LaunchPad

    public class TestingFacade {

        public TestingFacade(params LaunchPad[] lps) {
            this.EnlazarLaunchpad();
            this.SoundTest();
            this.ExportMix();
            this.AudioMixer();
            this.Codec();
            this.CompresionCodec();
            this.SoundWriter();
            this.FrequencyFilter();
            this.AudioConverter();
        }        

        // Enlazar Launchpads
        void EnlazarLaunchpad(){

            Console.WriteLine("Linking Launchpads");
        }
        //  Prueba de sonido
        void SoundTest(){

            Console.WriteLine("Testing sound");
        }
        // Exportacion de Mezclas 
        void ExportMix(){

            Console.WriteLine("Exporting Mix");
        }
        void AudioMixer(){

            Console.WriteLine("Conecting to Audio Mixer");
        }
        void Codec(){

            Console.WriteLine("Conecting to Codec");
        }
        void CompresionCodec(){

            Console.WriteLine("Conecting to Compresion Codec");
        }
        void SoundWriter(){

            Console.WriteLine("Conecting to SoundWriter");
        }
         void FrequencyFilter(){

            Console.WriteLine("Conecting to FrequencyFilter");
        }
         void AudioConverter(){

            Console.WriteLine("Conecting to AudioConverter");
        }


    }

}