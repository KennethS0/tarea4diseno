
/// This is an abstract class for the Launch Pad.
namespace LaunchPads {
    public abstract class LaunchPad {

        public abstract LaunchPad clone();

    }
}