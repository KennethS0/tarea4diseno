namespace LaunchPads {

/// Interface for the LaunchPad Builder.
    public interface ILaunchPadBuilder {
        void EnableHorizontalButtons(int cols);
        void EnableVerticalButtons(int rows);
        void EnableRGBLights();
        void EnableUSBTypeCPort();
        void EnableAudioJack();
        LaunchPad BuildLaunchPad();
        void Reset(LaunchPadType lpt);
    }

}