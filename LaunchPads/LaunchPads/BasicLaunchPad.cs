namespace LaunchPads {
///This is the basic LaunchPad, it inherits from the Abstract class LaunchPad
    public class BasicLaunchPad : LaunchPad {

        public BasicLaunchPad() {
            Console.WriteLine("Created Basic Launch Pad.");
        }

        public override LaunchPad clone(){
            Console.WriteLine("Cloning basic launchpad!"); 
            return new BasicLaunchPad();
        }
    }

}