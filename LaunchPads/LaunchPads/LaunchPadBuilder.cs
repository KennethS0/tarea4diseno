namespace LaunchPads {

    public enum LaunchPadType {
        BASIC
    }
///LaunchPad Builder, it builds only 1 Launchpad for all every call.
    public class LaunchPadBuilder : ILaunchPadBuilder {

        private LaunchPad launchpad;

        public LaunchPadBuilder(LaunchPadType lpt) {
            this.Reset(lpt);
        }

        private void GetLaunchPadInstance(LaunchPadType lpt) {
            LaunchPad lp = null;
            if (lpt == LaunchPadType.BASIC) {
                this.launchpad = new BasicLaunchPad();
            }
        }

        public void EnableHorizontalButtons(int cols) {
            Console.WriteLine($"{this.launchpad} has cols #{cols}");
        }

        public void EnableVerticalButtons(int rows) {
            Console.WriteLine($"{this.launchpad} has rows #{rows}");
        }
        
        public void EnableRGBLights() {
            Console.WriteLine($"{this.launchpad} has RGB Lights");
        }

        public void EnableUSBTypeCPort() {
            Console.WriteLine($"{this.launchpad} has USB Type C port");
        }
        
        public void EnableAudioJack() {
            Console.WriteLine($"{this.launchpad} has AudioJack");
        }
        
        public LaunchPad BuildLaunchPad() {
            Console.WriteLine($"Building {this.launchpad}");
            return this.launchpad;
        }
        
        public void Reset(LaunchPadType lpt) {
            this.GetLaunchPadInstance(lpt);     
        }
    }

}