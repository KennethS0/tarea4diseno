#pragma once

#include <bitset>
#include <list>

/// @brief  The TV gets analog signals 
namespace Televisores {

    class Televisor
    {
        public:
            void recibirSenhalAnalogica(std::list<int> senhal);
    };
}