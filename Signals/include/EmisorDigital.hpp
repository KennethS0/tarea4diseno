#pragma once

#include <bitset>
#include <list>

/// @brief it sends the signal, getting as an input a string and returning a list of groups of bits 
namespace Emisores {

    class EmisorDigital
    {
        public:
            std::list<std::bitset<4>> emitir(std::list<std::string> signal);
           

            // bool validar_signal(std::string signal);
    };
}