#pragma once

#include <bitset>
#include <list>

/// @brief This is the interface for the adaptar.
namespace Adapters 
{
    class ITelevisor
    {
        public:

            virtual void recibirSenhalDigital(std::list<std::bitset<4>> signal) = 0;
    };
}