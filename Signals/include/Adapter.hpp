#pragma once

#include <memory>
#include <iostream>

#include "Televisor.hpp"
#include "ITelevisor.hpp"
#include <list>


/// @brief The adapter gets the digital signal and turns it into a analog signal.
namespace Adapters 
{
    class Adapter : public ITelevisor
    {
        private:
            std::shared_ptr<Televisores::Televisor> televisor; 

        public:
            virtual void recibirSenhalDigital(std::list<std::bitset<4>> signal);
            
            Adapter(std::shared_ptr<Televisores::Televisor> tv) 
            {
                this->televisor = tv;
            }
    };
}