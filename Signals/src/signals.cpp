#include <iostream>
#include <memory>
#include "EmisorDigital.hpp"
#include "Televisor.hpp"
#include "Adapter.hpp"

int main() {
    // Instanciar Emisor Digital
    auto emisor = new Emisores::EmisorDigital();
    
    // Instanciar el tele
    auto tv = std::make_shared<Televisores::Televisor>();

    // Instanciar adaptador
    auto adapter = new Adapters::Adapter(tv);

    // Emitir señal
    auto senhal = emisor->emitir({"1001", "0110", "0001", "1010"});

    // Recibir señal
    adapter->recibirSenhalDigital(senhal);

    senhal = emisor->emitir({"0000", "0001", "0010", "0011"});
    adapter->recibirSenhalDigital(senhal);

    return 0;
}