#include <memory>
#include "Adapter.hpp"
#include <list>

void Adapters::Adapter::recibirSenhalDigital(std::list<std::bitset<4>> signal) {
    std::list<int> traduccion;

    std::cout << "Traduciendo senhal de digital a analogica..." << std::endl;
    for(int i=0 ; i<4; i++)
    {
        auto sign = signal.front();
        int trad = (int)(sign.to_ulong());
        signal.pop_front();
        traduccion.push_back(trad);
    }

    this->televisor->recibirSenhalAnalogica(traduccion); 
}
            