#include "EmisorDigital.hpp"
#include <bitset>
#include <iostream>
#include <list>

std::list<std::bitset<4>> Emisores::EmisorDigital::emitir(std::list<std::string> signal) {
    
    std::list<std::bitset<4>> senhal;

    std::cout << "Enviando senhal digital: ";
    for(int i=0 ; i<4; i++)
    {
        std::cout << signal.front() << " ";

        auto num = std::bitset<4>(std::string(signal.front())); 
        senhal.push_back(num);
        signal.pop_front();
    }

    std::cout << std::endl;
    return senhal;
}