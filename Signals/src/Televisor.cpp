#include <iostream>
#include "Televisor.hpp"
#include <list>

void Televisores::Televisor::recibirSenhalAnalogica(std::list<int> signal) {
    std::cout <<  "Recibiendo senhal analogica: ";
    for(int i=0 ; i<4; i++)
    {
        std::cout << signal.front() << " ";
        signal.pop_front();
    }
    std::cout << std::endl;
}