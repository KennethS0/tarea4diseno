#pragma once

#include <iostream>
#include "IAntenas.hpp"


/// @brief Abstract class for Vehicles.
namespace Vehicles
{
    class Vehicle
    {
        protected:
            Antenas::IAntena* antena;

        public:
            Vehicle(Antenas::IAntena* antena) 
            {
                this->antena = antena;
            }

            virtual void BuscarConexion() = 0;
    };
}