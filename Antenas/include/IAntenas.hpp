#pragma once


/// @brief Interface class for antenas.
namespace Antenas 
{
    class IAntena
    {
        public:

            virtual void enviarSenhal() = 0;
    };
}