#include "Vehicle.hpp"
#include "IAntenas.hpp"

/// @brief Vehicle type Automovil. 
namespace Vehicles {
    class Automovil : public Vehicle {
        private:
            int cuatrollantas;
        public:
            Automovil(Antenas::IAntena* antena) : Vehicle(antena) {        
                this->cuatrollantas = 4;
            }

            virtual void BuscarConexion();
    };
}