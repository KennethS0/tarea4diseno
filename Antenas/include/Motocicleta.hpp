#pragma once

#include "Vehicle.hpp"
#include "IAntenas.hpp"

/// @brief Vehilcle type Motocicleta.
namespace Vehicles
{
    class Motocicleta : public Vehicle
    {
        private:        
            int dosllantas;

        public:
                       
            Motocicleta(Antenas::IAntena* antena) : Vehicle(antena) {        
                this->dosllantas = 2;
            }

            virtual void BuscarConexion();
    };
}