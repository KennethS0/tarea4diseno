#pragma once

#include "IAntenas.hpp"
/// @brief Object tupe IAntena, more specifically a 5G one.
namespace Antenas
{
    class Antena5G : public IAntena
    {
        private:

        public:
            virtual void enviarSenhal();
            
            Antena5G() 
            {
                std::cout << "Creating 5G Antena..." << std::endl;
            }
    };
}