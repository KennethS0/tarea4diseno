#include "IAntenas.hpp"
#include "AntenaWAN.hpp"
#include "Antena5G.hpp"
#include "Vehicle.hpp"
#include "Automovil.hpp"
#include "Motocicleta.hpp"

int main() {
    ///Vehicle type automovil with a WAN antena.
    Vehicles::Vehicle* vehicle = new Vehicles::Automovil(new Antenas::AntenaWAN());
    vehicle->BuscarConexion();
    ///Vehicle type automovil with a 5G antena.
    vehicle = new Vehicles::Automovil(new Antenas::Antena5G());
    vehicle->BuscarConexion();    
    ///Vehicle type motocicleta with a 5G antena.
    vehicle = new Vehicles::Motocicleta(new Antenas::Antena5G());
    vehicle->BuscarConexion();
    ///Vehicle type motocicleta with a WAN antena.
    vehicle = new Vehicles::Motocicleta(new Antenas::AntenaWAN());
    vehicle->BuscarConexion();
    return 0;
}